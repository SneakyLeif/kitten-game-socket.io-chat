var socket = io("http://localhost:3000");

var name = "";

socket.on("name", function(data) {
    name = data;
    $("#chatNameInput").val(name);
});

$("#joinIRCButton").click(function() {
    chatName = $("#chatNameInput").val();
    socket.emit("joinIRC", chatName);

    $("#IRCNameDisplay").text(chatName);
    $("#joinIRCdiv").hide();
    $("#IRCdiv").show();

    name = chatName;
});

$("#messageSend").click(function() {
    messageSend();
});

$("#messageInput").keydown(function(e) {
    if (e.which == 13) {
        messageSend();
    }
});

function messageSend() {
    message = $("#messageInput").val();

    socket.emit("newMessage", message);
    $("#chatLog").append("<div class='chat-message'>"+ name +": "+ message +"</div>");

    $("#messageInput").val("");
}

socket.on("message", function(data) {
    console.log(data.from +": "+ data.message);
    $("#chatLog").append("<div class='chat-message'>"+ data.from +": "+ data.message +"</div>");
});