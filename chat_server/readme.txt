This is a node.js server which runs socket.io and an IRC client for each connected socket.
This allows the IRC chat to be displayed cleanly on the page and adjust for themes.
-------------------------
INSTALLATION INSTRUCTIONS

STEP 1: Install Node.js

	- https://nodejs.org/en/
	- I recommend choosing the LTS (Long-Term Support) version, but it'll probably work with the latest as well.

STEP 2: Change client IP

	- Open lib/chat.js, and change the first line from "localhost" to the public IP of whoever is hosting the server.
	- To find your public IP, google "what's my ip"
	- It should look something like: var socket = io("http://www.google.com:3000"); except without google, unless you're google, in that case, I'm famous!

STEP 3: Start server

	- Open a command console or powershell in the location of the server.js file
	- Type "node server" without quotes
	- Enjoy!