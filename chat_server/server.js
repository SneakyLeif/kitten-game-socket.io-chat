var io = require("socket.io");
var server = io.listen(3000);
var irc = require('irc');

var kittens = {};
var kittenList = [];

server.on("connection", function(socket) {

    // auto name kitten
    name = autoNameKitten();
    console.log(name +" connected!");

    // tell the client their kitten name
    socket.emit("name", name);

    socket.on("joinIRC", function(newName) {
        // SANITIZE NEWNAME HERE!!! DON'T FORGET!

        if (newName != kittens[socket.id].name) {
            removeFromKittenList(kittens[socket.id].name);
            console.log(kittens[socket.id].name +" changed their name to "+ newName +" and joined #kgtest!");
            kittens[socket.id].name = newName;
            kittenList.push(newName);
        } else {
            console.log(kittens[socket.id].name +" has joined #kgtest!");
        }

        // add an IRC client to the kitten
        kittens[socket.id].irc = new irc.Client('irc.canternet.org', kittens[socket.id].name, {
            channels: ['#kgtest'],
        });

        kittens[socket.id].irc.addListener('message', function (from, to, message) {
            if (to == "#kgtest") {
                data = {
                    from: from,
                    message: message
                };

                socket.emit("message", data);
            }
            //console.log(from + ' => ' + to + ': ' + message);
        });
    });

    socket.on("newMessage", function(message) {
        // SANITIZE MESSAGE AS WELL! DON'T FORGET!!!
        console.log(message);
        kittens[socket.id].irc.say("#kgtest", message);
    });

    function autoNameKitten() {
        kitten = {
            name: "",
            joinedIRC: false
        };

        nameFound = false;
        num = 1;

        while (!nameFound) {
            if (kittenList.includes("kitten_"+ num)) {
                num++;
            } else {
                kitten.name = "kitten_"+ num;
                nameFound = true;
                break
            }
        }
        
        kittenList.push(kitten.name);
        kittens[socket.id] = kitten;

        return kitten.name;
    }

    socket.on("disconnect", function() {
        console.log(kittens[socket.id].name +" disconnected.");

        // remove from kittenList
        removeFromKittenList(kittens[socket.id].name);

        // disconnect from irc client
        if (kittens[socket.id].hasOwnProperty("irc")) {
            kittens[socket.id].irc.disconnect();
        }

        // delete kitten from kittens object
        delete kittens[socket.id];
    });

    function removeFromKittenList(name) {
        index = 0;
        for (var i = 0; i < kittenList.length; i++) {
            if (kittenList[i] == name) {
                index = i;
                break;
            }
        }
        kittenList.splice(index, 1);
    }
});

